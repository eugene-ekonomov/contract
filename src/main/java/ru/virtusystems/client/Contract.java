package ru.virtusystems.client;

import com.google.gwt.user.client.ui.*;
import ru.virtusystems.client.ui.Contracts;
import com.google.gwt.core.client.EntryPoint;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class Contract implements EntryPoint {
  /**
   * The message displayed to the user when the server cannot be reached or
   * returns an error.
   */
  private static final String SERVER_ERROR = "An error occurred while "
      + "attempting to contact the server. Please check your network "
      + "connection and try again.";

  /**
   * This is the entry point method.
   */
  public void onModuleLoad() {
      Contracts contracts = new Contracts();

      RootPanel.get("contractsContainer").add(contracts);

  }
}

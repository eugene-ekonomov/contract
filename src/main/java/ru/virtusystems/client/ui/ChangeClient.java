package ru.virtusystems.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.datepicker.client.DateBox;

public class ChangeClient extends Composite {
    interface ChangeClientUiBinder extends UiBinder<HTMLPanel, ChangeClient> {
    }

    private static ChangeClientUiBinder ourUiBinder = GWT.create(ChangeClientUiBinder.class);

    public ChangeClient() {
        initWidget(ourUiBinder.createAndBindUi(this));
        birthDate.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
    }

    @UiField
    Button cancelButton;

    @UiField
    DateBox birthDate;
}
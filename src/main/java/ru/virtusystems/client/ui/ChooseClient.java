package ru.virtusystems.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;

public class ChooseClient extends Composite {
    interface ChooseClientUiBinder extends UiBinder<HTMLPanel, ChooseClient> {
    }

    private static ChooseClientUiBinder ourUiBinder = GWT.create(ChooseClientUiBinder.class);

    public ChooseClient() {

        initWidget(ourUiBinder.createAndBindUi(this));
        newClientButton.addClickHandler(event -> {
            final DialogBox dialogBox = new DialogBox();
            dialogBox.setAnimationEnabled(true);
            NewClient newClient = new NewClient();
            dialogBox.setWidget(newClient);

            newClient.cancelButton.addClickHandler(event1 -> dialogBox.hide());

            dialogBox.center();
        });
    }

    @UiField
    Button chooseClientButton;
    @UiField
    Button newClientButton;
    @UiField
    Button closeButton;
}
package ru.virtusystems.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.*;

public class Contracts extends Composite {
    interface ContractsUiBinder extends UiBinder<HTMLPanel, Contracts> {
    }

    private static ContractsUiBinder ourUiBinder = GWT.create(ContractsUiBinder.class);

    public Contracts() {

        initWidget(ourUiBinder.createAndBindUi(this));
        createButton.addClickHandler(clickEvent -> {
            final DialogBox dialogBox = new DialogBox();
            dialogBox.setAnimationEnabled(true);
            NewContract nc = new NewContract();
            dialogBox.setWidget(nc);

            nc.goToContracts.addClickHandler(event -> dialogBox.hide());
            dialogBox.center();
        });
    }

    @UiField
    Button createButton;
    @UiField
    Button openButton;
    @UiField
    Grid contractsTable;

}
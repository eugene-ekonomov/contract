package ru.virtusystems.client.ui;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.i18n.client.DateTimeFormat;
import com.google.gwt.i18n.client.impl.cldr.DateTimeFormatInfoImpl_aa;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.datepicker.client.DateBox;

public class NewContract extends Composite {
    interface NewContractUiBinder extends UiBinder<HTMLPanel, NewContract> {
    }

    private static NewContractUiBinder ourUiBinder = GWT.create(NewContractUiBinder.class);

    public NewContract() {
        initWidget(ourUiBinder.createAndBindUi(this));
        dateFrom.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
        dateTo.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
        dateContract.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
        birthDate.setFormat(new DateBox.DefaultFormat(DateTimeFormat.getFormat("dd-MM-yyyy")));
        chooseClient.addClickHandler(event -> {
            final DialogBox dialogBox = new DialogBox();
            dialogBox.setAnimationEnabled(true);
            ChooseClient cc = new ChooseClient();
            dialogBox.setWidget(cc);

            cc.closeButton.addClickHandler(event1 -> dialogBox.hide());
            dialogBox.center();
        });
        changeClient.addClickHandler(event -> {
            final DialogBox dialogBox = new DialogBox();
            dialogBox.setAnimationEnabled(true);
            ChangeClient cc = new ChangeClient();
            dialogBox.setWidget(cc);

            cc.cancelButton.addClickHandler(event12 -> dialogBox.hide());
            dialogBox.center();
        });
    }

    @UiField
    DateBox dateFrom;
    @UiField
    DateBox dateTo;
    @UiField
    DateBox dateContract;
    @UiField
    DateBox birthDate;

    @UiField
    Button goToContracts;
    @UiField
    Button chooseClient;
    @UiField
    Button changeClient;
}